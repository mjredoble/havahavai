import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: const Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                "Dubai Airport - DXB",
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              ),
               Text(
                "Dubai . 🇵🇸 United Arab Emirates",
                style: TextStyle(fontSize: 14.0),
              )
            ],
          ),
      ),
      body: _stackView()
    ));
  }

  Widget _stackView() {
    return Stack(
      children: [
        Container(
          color: Colors.white,
          width: double.infinity,
          height: double.infinity,
        ),
        Positioned(
          top: 20,
          left: 20,
          right: 20,
          child: _imageView(),
        ),
        Positioned(
          top: 310,
          right: 20,
          left: 20,
          child: _buttonRows(),
        ),
        Positioned(
          top: 430,
          right: 20,
          left: 20,
          child: _taxiServiceView(),
        ),
        // Positioned(
        //   bottom: 60,
        //   right: 80,
        //   child: Container(
        //     width: 120,
        //     height: 80,
        //     color: Colors.orange,
        //   ),
        // ),
      ],
    );
  }

  Widget _imageView() {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: 285,
          decoration: const BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.only(
              topLeft: Radius.circular(15.0),
              bottomRight: Radius.circular(15.0),
              bottomLeft: Radius.circular(15.0),
              topRight: Radius.circular(15.0),
              ))
        ),
        _cityView(),
        Positioned(
          top: 130,
          left: 10,
          right: 10,
          child: Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15.0),
                  bottomRight: Radius.circular(15.0),
                  bottomLeft: Radius.circular(15.0),
                  topRight: Radius.circular(15.0),
              )),
              width: 350.0,
              height: 140.0,
              child: Stack(
                children: [
                  _infoRowView(),
                ],
              )
            ),
        )
      ]);
  }
  
  Widget _cityView() {
    return const Padding(padding: EdgeInsets.all(5),
      child: AspectRatio(
          aspectRatio: 16 / 9,
          child: Image(
            image: AssetImage('assets/images/dubai.jpg'),
            fit: BoxFit.fill,
        ),
      ),
    );
  }

  Widget _infoRowView() {
    return Column(
    children: [
      const Row(
        children: [
          Expanded(
            child: SizedBox(
              height: 90.0,
              child: Center(
                  child: Padding(padding: EdgeInsets.all(4),child: CityColumnItem(icon: Icons.home, title: "19 C", subtitle: "Cloudy"),),
                )
            ),
          ),
          Expanded(
            child: SizedBox(
              height: 90.0,
              child:  Center(
                  child: Padding(padding: EdgeInsets.all(4),child: CityColumnItem(icon: Icons.calendar_month, title: "10 Mar", subtitle: "Sunday"),),
                )
            ),
          ),
          Expanded(
            child: SizedBox(
              height: 90.0,
              child: Center(
                  child: Padding(padding: EdgeInsets.all(4),child: CityColumnItem(icon: Icons.schedule, title: "8:45PM", subtitle: "GMT+4"),),
                )
            ),
          ),
          Expanded(
            child: SizedBox(
              height: 90.0,
              child: Center(
                  child: Padding(padding: EdgeInsets.all(4),child: CityColumnItem(icon: Icons.airplane_ticket, title: "AED", subtitle: "GMT+4"),),
                )
            ),
          ),
        ],
      ),
      const Divider(),
      _infoDirectionView(),
    ]);    
  }

    Widget _infoDirectionView() {
    return Row(
        children: [
          Expanded(
            child: SizedBox(
              height: 30.0, 
              child: Container(
                color: Colors.transparent,
                child: const Center(
                  child: Text('Get Directions'),
                ),
              ),
            ),
          ),
          Expanded(
            child: SizedBox(
              height: 30.0, 
              child: Container(
                color: Colors.transparent,
                child: const Center(
                  child: Text('Call Airport'),
                ),
              ),
            ),
          )
        ]
      );
    }

    Widget _buttonRows() {
      return Padding(
      padding: const EdgeInsets.all(5.0),
      child: Wrap(
        children: [
          CapsuleButton(
            label: 'Transport',
            onPressed: () {
              // TODO: pressed
            },
          ),
          const SizedBox(width: 8.0),
          CapsuleButton(
            label: 'Terminal',
            onPressed: () {
              // TODO: pressed
            },
          ),
          const SizedBox(width: 8.0),
          CapsuleButton(
            label: 'Forex',
            onPressed: () {
              // TODO: pressed
            },
          ),
          CapsuleButton(
            label: 'Contact Info',
            onPressed: () {
              // TODO: pressed
            },
          ),
        ],
      ),
    );
  }

  Widget _taxiServiceView() {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: 250,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(15.0),
              bottomRight: Radius.circular(15.0),
              bottomLeft: Radius.circular(15.0),
              topRight: Radius.circular(15.0),
              )
          )
        ),
        const Positioned(
          top: 20,
          left: 20,
          right: 20,
          child: Text(
            "Taxi Service",
            style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
          ),
        ),
        Positioned(
          top: 50,
          left: 20,
          right: 20,
          child: _taxiServicesView()
        ),
      ]);
  }

  Widget _taxiServicesView() {
      return const Padding(
      padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
      child: Wrap(
        runSpacing: 13.0,
        children: [
          TaxiServiceColumnItem(image: Image(
            image: AssetImage("assets/images/uber.png"),
            fit: BoxFit.fill)
            , title: "\$\$\$\$\$"),
            SizedBox(width: 8.0),
            TaxiServiceColumnItem(image: Image(
            image: AssetImage("assets/images/careem.png"),
            fit: BoxFit.fill)
            , title: "\$\$\$\$\$"),
            SizedBox(width: 8.0),
            TaxiServiceColumnItem(image: Image(
            image: AssetImage("assets/images/yango.png"),
            fit: BoxFit.fill)
            , title: "\$\$\$\$\$"),
            SizedBox(width: 8.0),
            TaxiServiceColumnItem(image: Image(
            image: AssetImage("assets/images/blacklane.png"),
            fit: BoxFit.fill)
            , title: "\$\$\$\$\$")
        ])
        );
  }
}

class CityColumnItem extends StatelessWidget {
  final IconData icon;
  final String title;
  final String subtitle;

  const CityColumnItem({super.key, 
    required this.icon,
    required this.title,
    required this.subtitle,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(icon, size: 28.0),
        const SizedBox(height: 8.0),
        Text(
          title,
          style: const TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
        ),
        Text(subtitle),
      ],
    );
  }
}

class CapsuleButton extends StatelessWidget {
  final String label;
  final VoidCallback onPressed;

  const CapsuleButton({super.key, 
    required this.label,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
        child: Text(label),
      ),
    );
  }
}

class TaxiServiceColumnItem extends StatelessWidget {
  final Image image;
  final String title;

  const TaxiServiceColumnItem({super.key, 
    required this.image,
    required this.title,
  });

  @override
  Widget build(BuildContext context) {
    return  Container(
          width: 92,
          height: 80,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(15.0),
              bottomRight: Radius.circular(15.0),
              bottomLeft: Radius.circular(15.0),
              topRight: Radius.circular(15.0),
              )
          ),
          child: Center(child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              image,
              const SizedBox(height: 8.0),
              Text(title, style: const TextStyle(fontSize: 10.0, fontWeight: FontWeight.bold, color: Colors.grey)),
            ],
          )),
        );
  }
}